<?php 
	require_once('data.php');
	session_start();

	// Bước 1: Lấy mã sản phẩm người dùng chọn
	$code = $_GET['code'];

	if(isset($_SESSION['cart'][$code])){
		$_SESSION['cart'][$code]['quantity']++;
	}else{
		//echo "$code";
		// Bước 2: Lấy thông tin sản phẩm dựa vào mã
		$product = $products[$code];
		$product['quantity'] = 1;
		// echo "<pre>";
		// 	print_r($product);
		// echo "</pre>";

		// Bước 3: Add sản phẩm vào giỏ hàng

		$_SESSION['cart'][$code] = $product;
	}

	// Bước 4: Chuyển về trang cart.php
	header('Location:cart.php');

 ?>