<?php 
	class Oto{
		var $ten;
		var $mau;
		var $so_cho;


		function chay(){
			echo "<br> Phương thức: Ôtô chạy !";
		}

		function inTT(){
			echo "<br> Thông tin xe !!!";
			echo "<br>Tên xe : " . $this->ten;
			echo "<br>Màu xe : " . $this->mau;
			echo "<br>Số chỗ : " . $this->so_cho;
		}

		function __construct(){
			echo "Constructor 2";
		}
	}
	
	$bmw = new Oto();

	$bmw->ten = "BMW X5";
	$bmw->mau = "Trắng";
	$bmw->so_cho = "5";

	$bmw->inTT();
	$bmw->chay();
 ?>