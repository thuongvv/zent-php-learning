<?php 
	require_once('connection.php');

	// Câu lệnh truy vấn
	$query = "SELECT * FROM categories";

	// Thực thi câu lệnh
	$result = $conn->query($query);

	// Tạo 1 mảng để chứa dữ liệu
	$categories = array();

	while($row = $result->fetch_assoc()) { 
		$categories[] = $row;
	}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Products</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
    	<h3 class="text-center">--- CATEGORIES ---</h3>
    	<a href="category_add.php" class="btn btn-success">Thêm mới</a>
        <table class="table">
        	<thead>
                <th> Name </th>
                <th> Description </th>
                <th> Image </th>
                <th>#</th>
            </thead>
        <?php foreach($categories as $cate){ ?>
            <tr>
                <td><?= $cate['name'] ?></td>
                <td><?= $cate['description'] ?></td>
                <td><img src="images/<?= $cate['thumbnail'] ?>" width="100px" height="100px"></td>
                <td>
                    <a href="category_detail.php?id=<?= $cate['id'] ?>" class="btn btn-primary">Xem</a>
                     <a href="category_edit.php?id=<?= $cate['id'] ?>" class="btn btn-default">Sửa</a>
                     <a href="category_delete.php?id=<?= $cate['id'] ?>" class="btn btn-warning">Xóa</a>
                </td>
            </tr>
        <?php } ?>
        </table>
    </div>
</body>
</html>