<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Products</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
    	<h3 class="text-center">--- CATEGORIES ---</h3>
    	<a href="index.php?mod=category&act=add" class="btn btn-success">Thêm mới</a>
        <?php 
            if(isset($_COOKIE['msg'])){
        ?>
            <div class="alert alert-primary" role="alert">
              <?= $_COOKIE['msg'] ?>
            </div>
        <?php       
            }
        ?>
        <table class="table">
        	<thead>
                <th> Name </th>
                <th> Description </th>
                <th> Image </th>
                <th>#</th>
            </thead>
        <?php foreach($categories as $cate){ ?>
            <tr>
                <td><?= $cate['name'] ?></td>
                <td><?= $cate['description'] ?></td>
                <td><img src="images/<?= $cate['thumbnail'] ?>" width="100px" height="100px"></td>
                <td>
                    <a href="index.php?mod=category&act=detail&id=<?= $cate['id'] ?>" class="btn btn-primary">Xem</a>
                     <a href="index.php?mod=category&act=edit&id=<?= $cate['id'] ?>" class="btn btn-default">Sửa</a>
                     <a href="index.php?mod=category&act=delete&id=<?= $cate['id'] ?>" class="btn btn-warning">Xóa</a>
                </td>
            </tr>
        <?php } ?>
        </table>
    </div>
</body>
</html>