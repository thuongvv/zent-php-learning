<?php 
	$mod = isset($_GET['mod'])?$_GET['mod']:''; // module
	$act = isset($_GET['act'])?$_GET['act']:'list'; // action

	switch ($mod) {
		case 'category':
			require_once('controllers/CategoryController.php');
			$controller_obj = new CategoryController();
			switch ($act) {
				case 'list':
					$controller_obj->list();
					break;
				case 'detail':
					$controller_obj->detail();
					break;
				case 'add':
					$controller_obj->add();
					break;
				case 'store':
					$controller_obj->store();
					break;
				case 'edit':
					$controller_obj->edit();
					break;
				case 'update':
					$controller_obj->update();
					break;
				case 'delete':
					$controller_obj->delete();
					break;
				default:
					$controller_obj->error();
					break;
			}
			break;
		case 'post':
			echo "<br> QUẢN LÝ BÀI VIẾT - POST";
			switch ($act) {
				case 'list':
					echo "<br> >>> XEM DANH SÁCH BÀI VIẾT";
					break;
				case 'add':
					echo "<br> >>> THÊM MỚI BÀI VIẾT";
					break;
				case 'edit':
					echo "<br> >>> SỬA BÀI VIẾT";
					break;
				
				default:
					echo "<br> >>> ACT 404";
					break;
			}
			break;
		case 'user':
			echo "<br> QUẢN LÝ NGƯỜI DÙNG - USER";
			break;
		
		default:
			echo "<br> Mod 404";
			break;
	}
	

 ?>